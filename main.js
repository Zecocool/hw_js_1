// 1. Оголосити змінну у JS можна 3-ма способами: let, var, const.
// 2. Різниця між prompt та confirm полягає у тому, що confirm - викликає на стороінці модальне віконце з текстом та 2 кнопками OK та Cencel і повертає відповідно True/False.
// У свою чергу prompt - викликає на стороінці модальне віконце з текстом, 2 кнопками OK.Cencel та рядок для введення користувачем тексту. Після чого повертає введене значення, або null при натисканні на сencel. 
// 3. Неявне перетворення типів - це процес конвертації значень між різноманітними типами автоматично. Зазвичай це відбувається при використанні значень різних типів НАПРИКЛАД
// true+false = 1 , 12/"6" = 2 , "number"+15+3 = "number153"

// Завдання 1

var admin;
var name ="Evgen";
admin = name;
console.log (admin)

// Завдання 2

var days= 3;
var oneMinute= 60;
var oneHour = 60;
var oneDay = 24;
var secondsOfDays = oneMinute * oneHour * oneDay * days;
console.log(secondsOfDays);

// Завдання 3

var filmname = prompt ("Enter your favorite film:", "It");
console.log ("Your favorite film is:" + filmname)
